<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'website');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u^!ytuNCepu)1VfjnfaU%_C(4K_{gN4rkd:)w#pY&ip$|7ICcAD~uB0TXR?Z$&!S');
define('SECURE_AUTH_KEY',  '?qDu6on7^Dhu|N F{UOC[YRc^R3M_WKf,6eHzYI& i|?)c+K YN+1!ME|Vwm`@8=');
define('LOGGED_IN_KEY',    'MI9r((y~:s$w#a@`1DIEV*P[q)7ewpPOO6:n$o00]!n+wz#3tOh)J h5hQs1d}QS');
define('NONCE_KEY',        'mn5VN(UB0[XJI$NZW.Y)z@+ZwW;2?LoEtB3*BA_*o?j$*=L57g|NB|>+Fv6Wl=:w');
define('AUTH_SALT',        '5_sK]?_?DK5fnRI.gr}JS[4/5i8VU1v{Diw:*Qoj|WC#:+s]Y&2xOt1RN9>p~$Xa');
define('SECURE_AUTH_SALT', ' a2r-S|@ANU:c;|Qr_0c,zRBN^2`V|s{^/R4C7Ln.aGozAwWt]fcwFfJ0wO-``@n');
define('LOGGED_IN_SALT',   'q/:,t2add+,ntvU#<Ntn5P5%HUdWB[UG{)Gg $4)4|ke{16Kl`!pV1-p11wC*+sB');
define('NONCE_SALT',       'Re>2P>KG$*u:mvgkAWTeW5BioMs46=.J`#(+Y~LnK/v&.A4Y`!#AC2|$(G_}so94');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
